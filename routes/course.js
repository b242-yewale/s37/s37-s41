const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const auth = require("../auth");

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all the courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(
		resultFromController));
});

// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
});

// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(
		resultFromController));
});

// Route for updating a course
// router.put("/:courseId", auth.verify, (req, res) => {
// 	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(
// 		resultFromController));
// });

// Route for deleting a course
router.delete("/:courseId", auth.verify, (req, res) => {

	const data = {
		reqpara : req.params,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	courseController.deleteCourse(data).then(resultFromController => res.send(
		resultFromController));
});

// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

	isAdmin = auth.decode(req.headers.authorization).isAdmin
	courseController.archiveCourse(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
	
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
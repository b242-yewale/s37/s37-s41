const Course = require("../models/Course");

// Create a new course
module.exports.addCourse = (data) => {

	// User is an Admin
	if (data.isAdmin){
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		})

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation failed 
			if(error){
				return false;
			}
			// Course creation successful
			else{
				return true;
			};
		});
	} else{
		return false;
	};
};


// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all Active courses
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		// Course is not updated
		if(error){
			return false;
		}
		// Course updated successfully
		else{
			return true;
		}
	})
}

// Delete a course
module.exports.deleteCourse = (data) => {

	if(data.isAdmin){
		return Course.findByIdAndDelete(data.reqpara.courseId).then((result, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
}

// Archive a course
module.exports.archiveCourse = (reqParams, reqBody, isUserAdmin) => {

	if(isUserAdmin){
		let updateActiveField = {
			isActive : reqBody.isActive
		};

		return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

			// Course not archived
			if (error) {

				return false;

			// Course archived successfully
			} else {

				return true;

			}

		});
	}
	else{
		return false;
	}

	
};


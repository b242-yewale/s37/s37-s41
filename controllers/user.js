const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		// No duplicate email found
		else{
			return false;
		}
	})
}

// User registration
module.exports.registerUser = (reqBody) => {
	// Creates a new user model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

// User Authentication(login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// user does not exist
		if(result == null){
			return false;
		}
		// User exists
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If passwords match/result of the code is true
			if(isPasswordCorrect){
				return {acces : auth.createAccessToken(result)};
			}
			// passwords donot match
			else{
				return false;
			}
		}
	})
}

// User search
module.exports.getProfile = (reqBody) => {
	return User.findOne({_id : reqBody._id}).then(result => {
		if (result == null){
			return false;
		}
		else{
			result.password = "";
			return result
		}
	})
}

// Enroll to a class
module.exports.enroll = async (data) => {

	if(data.isAdmin){
		// Add course Id in the enrollments array of the user
		let isUserUpdated = await User.findById(data.userId).then(user => {
			// Adds the courseId in the user's enrollments array
			user.enrollments.push({courseId: data.courseId});

			// Saves the updated user information in the database
			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Add the user ID in the enrollees array of the course
		let isCourseUpdated = await Course.findById(data.courseId).then(course => {
			// Add's the userId in the course's enrollees array
			course.enrollees.push({userId : data.userId});

			// Saves the updated course information in the database
			return course.save().then((course, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Condition that will check if the user and course documets have been updated
		if(isUserUpdated && isCourseUpdated){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
	
}